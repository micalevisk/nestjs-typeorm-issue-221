```bash
npm install

docker-compose up -d

# development
PORT=3131 npm run start:dev


curl localhost:3131
curl localhost:3131/one
curl -X POST localhost:3131/save
# ConnectionNotFoundError: Connection "default" was not found.
```


## Connect to the mysql database 

```bash
docker run --net=nest-typeorm-issue-984_default -it --rm mysql:8 mysql -h db -u root -p
# and type the password: root
```
