import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async Find() {
    return this.appService.Find();
  }

  @Get('one')
  async FindOne() {
    return this.appService.FindOne();
  }

  @Post('save')
  async Save() {
    return this.appService.Save();
  }
}
