import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { join } from 'path';
import { Characters } from './characters.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      name: 'charactersConnection',
      type: 'mysql',
      host: 'localhost',
      port: 53306,
      database: 'test-issue',
      username: 'root',
      password: 'root',
      synchronize: true,
      entities: [join(__dirname, '*.entity.{js, ts}')],
    }),
    TypeOrmModule.forFeature([Characters], 'charactersConnection'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
