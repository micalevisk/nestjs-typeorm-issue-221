import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Characters } from './characters.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Characters, 'charactersConnection')
    private readonly charactersRepository: Repository<Characters>,
  ) {
  }

  // Work Fine
  async Find() {
    return this.charactersRepository.find();
  }

  // Work Fine
  async FindOne() {
    return this.charactersRepository.findOne();
  }

  // Not Work
  async Save() {
    const characters = this.charactersRepository.create();
    characters.name = 'TypeOrm';
    console.log(characters)
    await characters.save();
  }
}
