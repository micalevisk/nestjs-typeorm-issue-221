import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('characters')
export class Characters extends BaseEntity
{
  @PrimaryGeneratedColumn()
  guid: number;

  @Column()
  name: string;
}
